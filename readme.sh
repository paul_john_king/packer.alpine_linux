#!/bin/sh
# vim:ff=unix tw=80 ts=4 sw=4 ai expandtab

set -e ;
set -u ;

exec 1> README.md ;

_echo_stdin(){

	local _line ;

	while IFS="" read -r _line ;
	do
		echo "${_line}" ;
	done ;

	return 0 ;

} ;

_main(){

	local _WARNING="\
The Bourne shell script \`readme.sh\` at the root of the project's directory
tree generates this document from the long help message of the script
\`create_alpine_linux_template\`.  In order to change this document, edit the
function \`_long_help\` of the script \`create_alpine_linux_template\`, set the
current working directory to the root of the project's directory tree, and call
\`readme.sh\`." ;

	_echo_stdin > "README.md" <<- __END_OF_STDIN__
<!--
${_WARNING}
-->

$(./create_alpine_linux_template -H)

\`readme.sh\`
-------------

${_WARNING}
__END_OF_STDIN__

	return 0 ;

} ;

_main "${@}" ;
