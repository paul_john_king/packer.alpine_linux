<!--
The Bourne shell script `readme.sh` at the root of the project's directory
tree generates this document from the long help message of the script
`create_alpine_linux_template`.  In order to change this document, edit the
function `_long_help` of the script `create_alpine_linux_template`, set the
current working directory to the root of the project's directory tree, and call
`readme.sh`.
-->

Summary
-------

This Bash script creates a Packer template that instructs Packer to try to
install a variant and version of the Alpine Linux operating system to a
`qcow2` disk-image file.  The script optionally prints the template, or calls
Packer to syntax-check or validate the template or build a disk-image file from
the template.

Usage
-----

The command call

    create_alpine_linux_template [-h] [-H] [-p «path»] [-k «path»] [-w] [-s] [-v] [-b] [-c] [-d «size»] [-t] [-g] «variant» «version»

creates a Packer template.  The template instructs Packer to try to download an
installation medium and a SHA-256 checksum for variant `«variant»` and
version `«version»` of the Alpine Linux operating system, and use a local
KVM/QEMU host to install the operating system from the medium to a disk-image
file in `qcow2` format.

*   `-h`  Write a short help message to standard output and exit.

*   `-H`  Write a long help message to standard output and exit.

*   `-p «path»`  Read a password from the file at path `«path»` and create
    a template that instructs Packer to set it as the console-login password
    for the user `root` of the disk image.  If the option is not used then
    the call asks the user for a password.  See the long help message for
    details of the password file.

*   `-k «path»`  Read public SSH keys from the file at path `«path»` and
    create a template that instructs Packer to append them to the authorised
    SSH keys file of the user `root` of the disk image.  This option can be
    used mutiple times.

*   `-w`  Write the template to standard output.  Warning: This option writes
    the password for the user `root` of the disk image to standard output.

*   `-s`  Call Packer to check the syntax of the template.

*   `-v`  Call Packer to validate the template.

*   `-b`  Call Packer to build the disk-image files from the template.

*   `-c`  Create a template that instructs Packer to display the console
    session while building the disk-image files.

*   `-d «size»`  Create a template that instructs Packer to set the size of
    the disk image to «size» GiB, where «size» is a positive integer without
    leading zeroes, though any disk-image files Packer builds from the template
    will almost certainly be much smaller than this.  If this option is not
    used then the template instructs Packer to set the size of the disk image
    to 2 GiB.

*   `-t`  Create a template that instructs Packer to append to the name of
    the disk image a UTC timestamp of the form `yyyymmddThhmmssZ`.

*   `-g`  Create a template that instructs Packer to append to the name of
    the disk image the first 7 characters of the commit ID of the Git
    repository for this command.

The disk image has the name

    alpine_linux_«variant»_«version»_«size»[_«timestamp»][_«commit_id»]

where `«size»` is the size of the disk image in GiB, `«timestamp»` is the
timestamp appended by the option `-t` if used, and `«commit_id»` is the
commit ID appended by the option `-g` if used.  If the option `-b` is used
and `«name»` is the name of the disk image then the command writes the
`qcow2` format disk-image file at the path `output_«name»/«name».qcow2`
relative to the current working directory.

Sources
-------

The template this command creates instructs Packer to try to download an
installation medium and a SHA-256 checksum for variant `«variant»` and
version `«major».«minor».«patch»` of the Alpine Linux operating system from

*   http://dl-cdn.alpinelinux.org/alpine/v«major».«minor»/releases/x86_64/alpine-«variant»-«major».«minor».«patch»-x86_64.iso, and

*   http://dl-cdn.alpinelinux.org/alpine/v«major».«minor»/releases/x86_64/alpine-«variant»-«major».«minor».«patch»-x86_64.iso.sha256

respectively.

Password File
-------------

If the option `-p «path»` is used then the password should appear on its own
on the first line of the file at the path `«path»`, and may contain
whitespace, but must be terminated with a new line (which is *not* part of the
password).  If `«path»` is relative then it is relative to the current
working directory.

Platforms
---------

We write the disk-images built by the template this command creates to machines
on a variety of platforms.  On first boot, if the `virgin_configuration`
service (see below) can identify the machine's platform then it performs a
one-time configuration of the virgin machine tailored to that platform.

The `atuin` platform denotes an `IBM System x3550` rack-mounted computer
with

*   2 `Intel Xeon 5140` 2.33 GHz CPUs, with 2 cores per socket and 1 thread
    per core (4 threads total);

*   17 GiB to 20 GiB of RAM;

*   `Broadcom NetXtreme II BCM5708` 1 Gb/s Ethernet interfaces `eth0` and
    `eth1`;

*   PXE enabled on Ethernet interface `eth0`; and

*   an `IBM ServeRAID` 293 GB hardware-RAID10 array `/dev/sda`, with 2x2
    `IBM HUC101414CSS300` 147 GB SAS 6.0 Gb/s 10 krpm drives.

The `cohen` platform denotes an `IBM System x3550 M3 7944-K3G` rack-mounted
computer with

*   2 `Intel Xeon E5620` 2.40 GHz CPUs, with 4 cores per CPU and 2 threads
    per core (16 threads total);

*   144 GiB of RAM;

*   `Broadcom NetXtreme II BCM5709` 1 Gb/s Ethernet interfaces `eth0`,
    `eth1`, `eth2` and `eth3`;

*   `Intel X540-AT2` 10 Gb/s Ethernet interfaces `eth4` and `eth5`;

*   PXE enabled on Ethernet interface `eth0`; and

*   an `LSI MegaRAID SAS 2108` 1.36 TB hardware-RAID10 array, with 3x2 `IBM
    ST9500530NS` 500 GB SATA 3.0 Gb/s 7200 rpm drives.

The `minion` platform denotes a `Supermicro X10SLE-F/HF` blade computer
with

*   1 `Intel Xeon E3-1230 v3` 3.30 GHz CPU, with 4 cores per CPU and 2
    threads per core (8 threads total);

*   16 GiB of RAM;

*   `Intel I350` 1 Gb/s Ethernet interfaces `eth0` and `eth1`;

*   PXE enabled on Ethernet interface `eth0`;

*   a `Samsung MZ7WD120HCFV-00003` 120 GB SATA 6.0 Gb/s solid-state drive
    `/dev/sda`; and

*   a `Western Digital Red WD7500BFCX-68N6GN0` 750 GB SATA 6.0 Gb/s 5400 rpm
    drive `/dev/sdb`.

The `tubul` platform denotes an `IBM System x3550 M3 7944-D4G` rack-mounted
computer with

*   2 `Intel Xeon E5620` 2.40 GHz CPUs, with 4 cores per CPU and 2 threads
    per core (16 threads total);

*   96 GiB of RAM;

*   `Emulex OneConnect (Skyhawk)` 10 Gb/s Ethernet interfaces `eth0` and
    `eth1`;

*   `Broadcom NetXtreme II BCM5709` 1 Gb/s Ethernet interfaces `eth2`,
    `eth3`, `eth4` and `eth5`;

*   PXE enabled on Ethernet interface `eth2`; and

*   an `LSI MegaRAID SAS 2108` 1.64 TB or 2.18 TB hardware-RAID10 array, with
    3x2 or 4x2 `IBM ST9600205SS` 600 GB SAS 6.0 Gb/s 10 krpm drives.

The `vimes` platform denotes a `Dell PowerEdge R430` rack-mounted computer
with

*   2 `Intel Xeon E5-2630L v4` 1.80 GHz CPUs, with 10 cores per CPU and 2
    threads per core (40 threads total);

*   128 GiB of RAM;

*   `Broadcom NetXtreme BCM5720` 1 Gb/s Ethernet interfaces `eth0`,
    `eth1`, `eth2` and `eth3`;

*   `Broadcom NetXtreme II BCM57810` 10 Gb/s Ethernet interfaces `eth4` and
    `eth5`;

*   PXE enabled on Ethernet interface `eth0`;

*   `Intel SSDSC2BB120G6R` 120 GB SATA 6.0 Gb/s solid-state drives
    `/dev/sda` and `/dev/sdb`;

*   `Intel 3710 Series SSDSC2BA400G4R` 400 GB SATA 6.0 Gb/s solid-state
    drives `/dev/sdc` and `/dev/sde`; and

*   a 15GB hardware-RAID1 array `/dev/sdd`, with 2 15 GB SD cards.

Template
--------

The template has one builder and four provisioners.  The builder instructs
Packer to

*   install the base Alpine Linux operating system, giving the user `root`
    the password given on the command line or read from a password file, and

*   configure the SSH daemon to permit `root` login with a password, and so
    enable SSH access for the provisioners.

The first provisioner instructs Packer to create certain file-system paths in
the disk image.  The second provisioner instructs Packer to copy the entire
file-system tree under the path `content` of the application to the root path
of the disk image.  The third provisioner instructs Packer to copy the public
SSH keys read from key files to the path `/root/.ssh/authorized_keys` of the
disk image.  The fourth provisioner instructs Packer to

*   export some variables of the application's shell to the provisioner's
    shell,

*   perform in-place shell expansion of an image information file,

*   install some Alpine Linux packages,

*   create symbolic links for some services and DHCP handlers,

*   secure the authorised SSH keys of the user `root`, and

*   delete an obsolete file and the current host private SSH keys (these will
    be regenerated during the next boot).

The outcome for any disk image produced by these instructions is as follows.  

#### Image info

The file at the path `/etc/image_info` contains shell variable assigments
that describe the disk image and its creation.

#### `/usr/local/bin`

The files at the paths

*   `/usr/local/bin/expand_parameters`,

*   `/usr/local/bin/get_image_info`,

*   `/usr/local/bin/get_interfaces`,

*   `/usr/local/bin/get_platform`,

*   `/usr/local/bin/run_ntpd`,

*   `/usr/local/bin/set_hostname`, and

*   `/usr/local/bin/set_issue_and_motd`

contain in-house utility scripts.

The command call

    expand_parameters «path»

performs in-place Bourne shell parameter expansion, command substitution and
arithmetic expansion on the contents of the file at the path '«path»'.

The command call

    get_image_info

writes to standard output a description of the disk image and its creation.

The command call

    get_interfaces

writes to standard output a formatted list of the machine's network interfaces
and their IPv4 and IPv6 addresses.  `get_interfaces` uses Busybox `ip` to
find the network interfaces and their addresses, but Busybox `ip` and hence
`get_interfaces` return no IPv6 addresses while the machine is booting.

The command call

    get_platform

writes to standard output the machine's platform if discoverable, "?"
otherwise.  The command requires that the Alpine Linux package `dmidecode` be
installed.

The command call

    run_ntpd «hostname_1» ... «hostname_n»

tries to ensure that a single NTP daemon is running and that the daemon queries
NTP services running on the hosts with the names `hostname_1`, ...  and
`hostname_n`.  More exactly, if the call has no arguments -- that is, the
call has been given no NTP services to query -- then an NTP daemon is
unnecessary and the call simply tries to terminate every instance of every
`/usr/sbin/ntpd ...` process.  If the call has arguments -- that is, the call
has been given at least one NTP service to query -- then the call searches for
an instance of the process

    /usr/sbin/ntpd -N -p «hostname_1» ... -p «hostname_n»

If it finds such an instance then it lets the first found instance continue to
run but tries to terminate every instance of every `/usr/sbin/ntpd ...`
process other than the found instance.  If it finds no such instance then it
tries to terminate every instances of every `/usr/sbin/ntpd ...` process and
start a single, new instance of the sought process.

The command call

    set_hostname «name»

writes `«name»` to the file at path `/etc/hostname` and sets the name of
the machine to `«name»`.  The command call

    set_hostname

reads a name from the file at path `/etc/hostname` and sets the name of the
machine to that name.

The command call

    set_issue_and_motd

writes to the prelogin and login message files `/etc/issue` and `/etc/motd`
descriptions of the disk image and its creation, and the machine's name,
platform, kernel and network interfaces.

#### Network interfaces

The file at the path `/etc/network/interfaces` contains an incomplete and
inefficient physical-interface configuration for an arbitrary machine.  It
should enable `ifup` to bring up at boot and configure via DHCP at least one
physical interface on each machine on each platform that we support.

On first boot, the `virgin_configuration` service (see below) overwrites the
contents of the file with a physical-interface configuration tailored to the
platform on which the machine is running.

#### Virgin configuration

The file at the path `/root/virgin_configuration` contains a one-shot virgin
configuration script.  First, the script tries to identify the platform that
the machine is running on.  Next, if the script can identify the platform then
it

*   overwrites the contents of the file at the path `/etc/network/interfaces`
    with a configuration appropriate for the platform's physical-interfaces
    that also sources files at the paths `/etc/network/interfaces.vlan` and
    `/etc/network/interfaces.bridge`,

*   ensures files exist at the paths `/etc/network/interfaces.vlan` and
    `/etc/network/interfaces.bridge` in which other processes may write
    respectively VLAN- and bridge-interface configurations,

*   creates LVM2 volume groups `alpine` and (if the platform has faster
    drives) `alpine_fast` on the platform's drives, and

*   creates a swap space on an LVM2 logical volume `swap` on the LVM2 volume
    group `alpine_fast` (if it exists) or `alpine`;

otherwise it

*   overwrites the contents of the file at the path `/etc/network/interfaces`
    with a configuration that brings up at boot the loopback device and the
    physical interface `eth0` and tries to configure `eth0` via DHCP.

Finally, the script deletes itself.

The file at the path `/etc/init.d/virgin_configuration` contains the control
script for the one-shot service `virgin_configuration`.  The fourth Packer
provisioner creates a symbolic link that starts the service on entering
runlevel `boot`, and the control script itself ensures that the service is
started before the service `networking` starts.  On the machine's first boot,
the script calls the one-shot script at the path `/root/virgin_configuration`
in order to complete the configuration of the machine.  It then deactivates and
deletes itself.  On the machine's subsequent boots, neither the script
`/root/virgin_configuration` nor the service `virgin_configuration` nor the
service-activating link exist.

#### Networking

The file at the path `/etc/init.d/networking` contains the control script for
the service `networking`.  Alpine Linux starts the service on entering
runlevel `boot`.

Alpine Linux has a control script for the service, but while the script
executes `auto` stanzas in the file `/etc/network/interfaces`, it does not
execute `auto` stanzas included by `source` stanzas in this file.  To
circumvent this, the file `/etc/init.d/networking` replaces the provided
control script with a simpler script that calls `ifup -a` and `ifdown -a`.

#### Host name

The file at the path `/etc/init.d/hostname` contains the control script for
the service `hostname`.  Alpine Linux starts the service on entering runlevel
`boot`.  On start, the service calls the command `set_hostname` (see above)
to read a name from the file at path `/etc/hostname` and set the name of the
machine to that name.  The service no longer uses the service configuration
file at the path `/etc/conf.d/hostname`, so this file is deleted.

The file at the path `/etc/udhcpc/post-bound/hostname` and its symbolic link
at the path `/etc/udhcpc/post-renew/hostname` contain a handler script for
DHCP lease-binding and lease-renewal events.  If these events supply a name for
the machine then the script calls the command `set_hostname` in order to
write the name to the file at path `/etc/hostname` and set the name of the
machine to that name, ready for the next time the machine boots.

#### Issue

The file at the path `/etc/init.d/issue_and_motd` contains the control script
for the service `issue_and_motd`.  The fourth Packer provisioner creates a
symbolic link that starts the service on entering runlevel `boot`.  On start,
the service calls the command `set_issue_and_motd` (see above) in order to
write to the prelogin and login message files `/etc/issue` and `/etc/motd`
descriptions of the disk image and its creation, and the machine's name,
platform, kernel and network interfaces.

The file at the path `/etc/udhcpc/post-bound/issue_and_motd` and its symbolic
link at the path `/etc/udhcpc/post-renew/issue_and_motd` contain a handler
script for DHCP lease-binding and lease-renewal events.  The script also calls
the command `set_issue_and_motd` in order to write to the prelogin and login
message files `/etc/issue` and `/etc/motd` possibly updated descriptions of
the disk image and its creation, and the machine's name, platform, kernel and
network interfaces.

#### NTP daemon

The file at the path `/etc/udhcpc/post-bound/ntpd` and its symbolic link at
the path `/etc/udhcpc/post-renew/ntpd` contain a handler script for DHCP
lease-binding and lease-renewal events.  The script calls the command
`run_ntpd` (see above) with a list of any NTP-host names supplied by the
event in order to try to ensure that a single NTP daemon is running and that
the daemon queries NTP services running on the hosts with the supplied names.

#### SSH

The file at the path `/etc/ssh/sshd_config` contains the SSH daemon
configuration.  It permits login by the user `root` (but not with password or
keyboard-interactive authentication), reads a user's authorised public keys
from the file at the path `.ssh/authorized_keys` in the user's home
directory, and enables the SFTP subsystem.

The fourth provisioner deletes all existing public and private SSH keys in the
directory at the path `/etc/ssh` in order to force each instance of this
image to generate its own set of keys on first boot.

#### Screen

The file at the path `/root/.screenrc` contains the `screen` configuration
for the user `root`.  It disables the copyright notice during `screen`
startup and provides a green message line displaying the window number, the
window title, the machine name and the current working directory.

`readme.sh`
-------------

The Bourne shell script `readme.sh` at the root of the project's directory
tree generates this document from the long help message of the script
`create_alpine_linux_template`.  In order to change this document, edit the
function `_long_help` of the script `create_alpine_linux_template`, set the
current working directory to the root of the project's directory tree, and call
`readme.sh`.
